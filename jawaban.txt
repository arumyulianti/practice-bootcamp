1. Membuat database
	CREATE DATABASE myshop;

2. Membuat table
	- table users
		CREATE TABLE users (
		id int PRIMARY KEY AUTO_INCREMENT,
		name varchar(255),
		email varchar(255),
		password varchar(255));
	
	- table items
		CREATE TABLE items (
		id int PRIMARY KEY AUTO_INCREMENT,
		name varchar(255),
		description varchar(255),
		price int,
		stock int,
		category_id int);
		
		menambahkan foreign key :
		ALTER TABLE items ADD FOREIGN KEY (category_id) REFERENCES categories(id);
		
	-table categories
		CREATE TABLE categories (
		id int PRIMARY KEY AUTO_INCREMENT,
		name varchar(255));

3. Memasukkan data pada table
	- table users
		INSERT INTO users VALUES 
			(null,'Jhon Doe','john@doe.com','john123'), 
			(null,'Jhon Doe','john@doe.com','jenita123');
	
	- table items
		INSERT INTO items VALUES 
			(null,'Sumsang b50','hape keren dari merek sumsang','4000000',100,1), 
			(null,'Uniklooh','baju keren dari brand ternama','500000',50,2),
            (null,'IMHO Watch','jam tangan anak yang jujur banget','2000000',10,1);
			
	- table categories
		INSERT INTO categories VALUES 
			(null,'gadget'), 
            (null,'cloth'),
            (null,'men'),
            (null,'woman'),
            (null,'branded');
			
4. Mengambil data dari database
	- mengambil data users tanpa password
		SELECT id, name, email FROM users;
	
	- mengambil data items
		SELECT * FROM items WHERE price = '1000000';
		
		SELECT * FROM items WHERE name LIKE '%uniklo%';
		
	- Menampilkan data items join dengan kategori
		SELECT items.name, items.description, items.price, items.stock, items.category_id, categories.name FROM items LEFT JOIN categories ON items.category_id = categories.id;
		
5. Mengubah Data dari Database
		UPDATE items set price = 2500000 WHERE name = 'Sumsang b50';
	
		